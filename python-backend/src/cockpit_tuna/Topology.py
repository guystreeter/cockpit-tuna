#!/usr/bin/python

from __future__ import annotations
import errno
from pathlib import Path
import re
import json
from typing import Any, Dict, List, Iterator, Union


_TrailingDigitsRegex = re.compile(r"^(?P<root>.*[^\d])(?P<index>\d+)$")


class _dirEntry(object):
    def __init__(self, thisPath: Union[_dirEntry, Path, str]):
        if isinstance(thisPath, _dirEntry):
            self._currentPath = thisPath._currentPath
        else:
            assert len(str(thisPath)) > 0
            self._currentPath = Path(thisPath)
        self._dict = {}

    def _loadDict(self):
        newDict = {}
        for path in self._currentPath.iterdir():
            if match := _TrailingDigitsRegex.match(path.name):
                newPath = self._currentPath / match.group("root")
                newDict[newPath.name] = _indexedEntry(newPath.resolve())
            if path.is_file():
                try:
                    with open(path, "r") as f:
                        try:
                            value = f.read().strip()
                        except OSError as e:
                            if int(e.errno) == errno.EIO:
                                continue
                            raise e
                except PermissionError:
                    continue
            elif path.is_dir():
                value = _dirEntry(path.resolve())
            else:
                raise NotImplementedError
            newDict[path.name] = value
        self._dict = newDict
        # print(newDict)

    def _getDict(self) -> Dict:
        if len(self._dict) == 0:
            self._loadDict()
        return self._dict

    def __repr__(self) -> str:
        return f"<_dirEntry {self._currentPath}>"

    def __str__(self) -> str:
        return self._currentPath.name

    def __getattr__(self, name: str) -> Any:
        try:
            return self._getDict()[name]
        except KeyError:
            pass
        return self.__getattribute__(name)

    def __getitem__(self, __name: str) -> Any:
        return self._getDict()[__name]

    def __iter__(self) -> Iterator[_dirEntry]:
        return iter(getattr(self, path.name) for path in self._getDict())

    def keys(self) -> List:
        return list(path for path in self._getDict())

    def __truediv__(self, value: str) -> _dirEntry:
        """join a name to the path"""
        path = (self._currentPath / value).resolve()
        if path.exists():
            return _dirEntry(path)
        return _indexedEntry((self._currentPath.parent / value).resolve())

    def __floordiv__(self, value) -> _dirEntry:
        """join a name to the parent path"""
        path = (self._currentPath.parent / value).resolve()
        if path.exists():
            return _dirEntry(path)
        return _indexedEntry((self._currentPath.parent / value).resolve())


class _indexedEntry(_dirEntry):
    def __init__(self, prefix: Path):
        super().__init__(prefix.parent)
        self._prefix = prefix.name

    def __repr__(self) -> str:
        return f"<_indexedEntry {self._currentPath}/{self._prefix}[X]>"

    def __getattr__(self, key: str) -> Any:
        return super().__getattr__(key)

    def __getitem__(self, __index: str) -> Any:
        path = (self._currentPath / (self._prefix + str(__index))).resolve()
        return super().__getitem__(path.name)

    def keys(self) -> List[str]:
        """sorted list of trailing numerics"""
        keys = []
        for path in self._currentPath.glob(self._prefix + "*"):
            if match := _TrailingDigitsRegex.match(path.name):
                keys.append(match.group("index"))
        return sorted(list(keys))

    def __iter__(self) -> Iterator[str]:
        return iter(self[key] for key in self.keys())


def _rangesToList(inputString: str) -> List:
    result = []
    for part in inputString.split(","):
        if "-" in part:
            first, last = part.split("-")
            result.extend(range(int(first), int(last) + 1))
        elif len(part) != 0:
            result.append(int(part))
    return result


def _listToRanges(intList):
    result = []
    for value in sorted(intList):
        if not result or result[-1][-1] + 1 != value:
            result.append([value])
        else:
            result[-1].append(value)
    return ",".join(
        [
            str(x[0]) if len(x) == 1 else str(x[0]) + "-" + str(x[-1])
            for x in result
        ]
    )


class Cache(_dirEntry):
    """create from an index dir of a cache dir of a cpu"""

    def __init__(self, dirEntry: Union[_dirEntry, str]):
        super().__init__(dirEntry)

    def __repr__(self) -> str:
        return (
            f"<{self.type} level {self.level} "
            f"Cache {self.id} of CPUs {self.shared_cpu_list}>"
        )

    def __str__(self) -> str:
        return f"L{self.level} {self.type} Cache {self.id}"

    @property
    def SharedCPUs(self) -> List[PU]:
        cpus = self // "../../cpu"
        return [PU(cpus[n]) for n in _rangesToList(self.shared_cpu_list)]

    @property
    def SharedCores(self) -> List[Core]:
        ids = []
        cores = []
        for cpu in self.SharedCPUs:
            core = Core(cpu.topology)
            if core.ID not in ids:
                ids.append(core.ID)
                cores.append(core)
        return cores


class Caches(_dirEntry):
    """create from the cache subdir of a cpu"""

    def __init__(self, thisPath: Union[_dirEntry, str]):
        super().__init__(thisPath)

    def __repr__(self) -> str:
        return f"<Caches of {self._currentPath.parent.name}>"

    def __str__(self) -> str:
        return str(self._currentPath)

    def __iter__(self) -> Iterator[Cache]:
        caches = self.index
        return iter(Cache(cache) for cache in caches)


class Core(_dirEntry):
    """create from the topology subdir of a cpu"""

    def __init__(self, dirEntry: Union[_dirEntry, str]):
        super().__init__(dirEntry)

    def __repr__(self) -> str:
        return f"<Core {self.core_id}>"

    def __str__(self) -> str:
        return str(self._currentPath)

    @property
    def ID(self) -> int:
        return int(self.core_id)

    @property
    def PUs(self) -> List[PU]:
        cpus = self // "../cpu"
        return [PU(cpus[n]) for n in _rangesToList(self.thread_siblings_list)]


class PU(_dirEntry):
    """create from a cpu indexed dir"""

    def __init__(self, dirEntry: Union[_dirEntry, str]):
        super().__init__(dirEntry)
        match = _TrailingDigitsRegex.match(self._currentPath.name)
        assert match is not None
        self._number = int(match.group("index"))

    def __repr__(self) -> str:
        return f"<PU {self.ID}>"

    def __str__(self) -> str:
        return str(self._currentPath)

    @property
    def Caches(self) -> List[Cache]:
        index = self.cache.index
        return [Cache(index[key]) for key in reversed(index.keys())]

    @property
    def Core(self) -> Core:
        return Core(self.topology)

    @property
    def ID(self) -> int:
        return self._number

    @property
    def Socket(self) -> Socket:
        return Socket(self.topology)


class Socket(_dirEntry):
    """create from the topology subdir of a cpu"""

    def __init__(self, dirEntry: Union[_dirEntry, str]):
        super().__init__(dirEntry)
        self._number = int(self.physical_package_id)

    def __repr__(self) -> str:
        return f"<Socket {self.Number}>"

    def __str__(self) -> str:
        return str(self._currentPath)

    def __eq__(self, other: Socket) -> bool:
        return self.Number == other.Number

    @property
    def Number(self) -> int:
        return self._number

    @property
    def CPUs(self) -> List[PU]:
        cpus = self // "../cpu"
        cpuList = []
        for key in cpus.keys():
            s = Socket(cpus[key].topology)
            if s == self:
                c = PU(cpus[key])
                if c.Socket == self:
                    cpuList.append(c)
        return cpuList

    @property
    def Cores(self) -> List[Core]:
        cores = {}
        for cpu in self.CPUs:
            core = cpu.Core
            if core.ID not in cores:
                cores[core.ID] = core
        return list(cores.values())


class Node(_dirEntry):
    """create from /sys/devices/system/node/nodeX"""

    def __init__(self, dirEntry: Union[_dirEntry, str]):
        super().__init__(dirEntry)
        match = _TrailingDigitsRegex.match(self._currentPath.name)
        assert match is not None
        self._number = int(match.group("index"))

    def __repr__(self) -> str:
        return f"<Node {self.Number} with CPUs {self.cpulist}>"

    def __str__(self) -> str:
        return str(self._currentPath)

    @property
    def Number(self) -> int:
        return self._number

    @property
    def CPUs(self) -> List[PU]:
        cpus = self.cpu
        return [PU(cpus[key]) for key in cpus.keys()]

    @property
    def Sockets(self) -> List[Socket]:
        sockets = {}
        for cpu in self.CPUs:
            s = Socket(cpu.topology)
            if s.Number in sockets:
                continue
            sockets[s.Number] = s
        return list(sockets.values())


class Nodes(_dirEntry):
    """create from /sys/devices/system/node"""

    def __init__(self, dirEntry: Union[_dirEntry, str]):
        super().__init__(dirEntry)

    def __str__(self) -> str:
        return str(self._currentPath)

    def __repr__(self) -> str:
        return "<Nodes>"

    def __iter__(self) -> Iterator[Node]:
        nodes = self.node
        return iter(Node(node) for node in nodes)


# so we can pont it elsewhere for testing
_basePath = _dirEntry("/sys/devices/system")

_onlineCPUs = _rangesToList(_basePath.cpu.online)

_cacheTypeToLetter = {
    "Unified": "",
    "Data": "d",
    "Instruction": "i",
}


def _CPU(cpu: PU) -> Dict:
    return {"online": cpu.ID in _onlineCPUs}


def _coreCPUs(core: Core) -> Dict:
    return {cpu.ID: _CPU(cpu) for cpu in core.PUs}


def _cacheStr(cache: Cache) -> str:
    return f"L{cache.level}{_cacheTypeToLetter[cache.type]} Cache {cache.id}"


def _cachesBelowCore(core: Core) -> Dict:
    coreCaches = {}
    coreCpus = _rangesToList(core.thread_siblings_list)
    for cpu in core.PUs:
        for cache in cpu.Caches:
            cacheCpus = _rangesToList(cache.shared_cpu_list)
            extras = set(cacheCpus) - set(coreCpus)
            label = {"Label": _cacheStr(cache), 'CPUList': cacheCpus}
            if len(extras) == 0:
                if cache.level not in coreCaches:
                    coreCaches[cache.level] = {cache.type: {cache.id: label}}
                if cache.type not in coreCaches[cache.level]:
                    coreCaches[cache.level][cache.type] = {cache.id: label}
                if cache.id not in coreCaches[cache.level][cache.type]:
                    coreCaches[cache.level][cache.type][cache.id] = label
    return coreCaches


def _cachesAboveCore(core: Core) -> List[Cache]:
    coreCaches = {}
    for cpu in core.PUs:
        for cache in cpu.Caches:
            if len(cache.SharedCores) > 1:
                coreCaches[cache.id] = cache
    return list(coreCaches.values())


def _socketDict(sockets: List[Socket]) -> Dict:
    dict = {}
    for socket in sockets:
        socketDict = {}
        cacheDict = {}
        coreDict = {}
        for core in socket.Cores:
            caches = _cachesAboveCore(core)
            assert len(caches) < 2
            if len(caches) > 0:
                cache = caches[0]
                label = {
                    "Label": _cacheStr(cache),
                    'CPUList': _rangesToList(cache.shared_cpu_list)
                }
                if cache.level not in cacheDict:
                    cacheDict[cache.level] = {cache.type: {cache.id: label}}
                elif cache.type not in cacheDict[cache.level]:
                    cacheDict[cache.level][cache.type] = {cache.id: label}
                elif cache.id not in cacheDict[cache.level][cache.type]:
                    cacheDict[cache.level][cache.type][cache.id] = label
            coreDict[core.ID] = {
                'Caches': _cachesBelowCore(core),
                'CPUs': _coreCPUs(core),
                'CPUList': _rangesToList(core.thread_siblings_list)
            }
        if len(cacheDict) > 0:
            socketDict["Caches"] = cacheDict
        if len(coreDict) > 0:
            socketDict["Cores"] = coreDict
        socketDict['CPUList'] = [cpu.ID for cpu in socket.CPUs]
        dict[socket.Number] = socketDict
    return dict


def _nodeDict(nodes: Nodes) -> Dict:
    dict = {}
    cpulist = []
    for node in nodes:
        sockets = _socketDict(node.Sockets)
        cpus = _rangesToList(node.cpulist)
        cpulist += cpus
        dict[node.Number] = {
            "Sockets": sockets,
            'CPUList': cpus
        }
    return {"Nodes": dict, 'CPUList': sorted(list(set(cpulist)))}


def GetTopology() -> Dict:
    return _nodeDict(Nodes(_basePath.node))


if __name__ == "__main__":
    print(json.dumps(GetTopology(), indent=4))
