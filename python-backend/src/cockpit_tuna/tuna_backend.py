#!/usr/bin/env python3

# Copyright 2022-2023 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

import argparse
import json
import os
import sys
import errno
import re
from typing import Dict

import procfs

from . import Topology


def _handleUpdate(args: argparse.Namespace):
    update = {}

    pidstats = procfs.pidstats()
    pidstats.reload_threads()

    policyList = {
        "OTHER": os.SCHED_OTHER,
        "FIFO": os.SCHED_FIFO,
        "RR": os.SCHED_RR,
        "BATCH": os.SCHED_BATCH,
        "IDLE": os.SCHED_IDLE,
        "ISO": 4,
        "DEADLINE": 6,
    }

    num2policy = {v: k for k, v in policyList.items()}

    def is_kernel_thread(process_):
        filename = "/proc/%d/smaps" % (process_.pid,)
        try:
            with open(filename) as smapsfile:
                line = smapsfile.readline()
        except IOError as error:
            if error.errno == errno.EACCES:
                # This doesn't work without privileges.
                return False
            if error.errno == errno.ENOENT:
                # The process has gone away
                return False
            raise error
        if line:
            return False
        pidstat = procfs.pidstat(process_.pid)
        if pidstat["state"] == "Z":
            return False
        return True

    def schedStr(pid):
        pol = os.sched_getscheduler(pid) & ~os.SCHED_RESET_ON_FORK
        return num2policy[pol]

    def buildProcessEntry(process: procfs.procfs.process) -> Dict:
        pid = process["pid"]
        policy = schedStr(pid)

        def arr2hex(array: set[int]) -> str:
            result = 0
            for value in array:
                result |= 1 << value
            return f"0x{result:x}"

        affinity = arr2hex(os.sched_getaffinity(pid))
        rtPriority = process["stat"]["rt_priority"]
        lastCPU = process["stat"]["processor"]
        vol = process["status"]["voluntary_ctxt_switches"]
        nonVol = process["status"]["nonvoluntary_ctxt_switches"]
        commandLine = procfs.process_cmdline(process)
        isKthread = is_kernel_thread(process)
        return {
            "pid": pid,
            "policy": policy,
            "rtPriority": rtPriority,
            "affinity": affinity,
            "lastCPU": lastCPU,
            "volCtxSwitch": vol,
            "nonVolCtxSwitch": nonVol,
            "commandLine": commandLine,
            "isKthread": isKthread,
        }

    procs = []
    for key in pidstats.keys():
        process = pidstats[key]
        try:
            entry = buildProcessEntry(process)
        except (ProcessLookupError, FileNotFoundError):
            # happens if the process went away already
            continue
        if "threads" in process:
            tids = []
            for tid in process.threads.keys():
                try:
                    tids += [buildProcessEntry(process.threads[tid])]
                except (ProcessLookupError, FileNotFoundError):
                    continue
            if tids:
                entry["threads"] = tids
        procs += [entry]

    update["Processes"] = procs

    cpus = {}
    cs = procfs.cpusstats()
    for cpu in cs:
        usage = (cs[cpu].user + cs[cpu].nice + cs[cpu].system) / cs.hertz
        if cpu == 0:
            cpu = "total"
        else:
            cpu -= 1
        cpus[cpu] = usage
    update["CPUstats"] = {"time": cs.time, "cpus": cpus}

    baseNicPath = "/sys/class/net"
    nicList = os.listdir(baseNicPath)
    nics = {}
    for nic in nicList:
        nicPath = os.path.join(baseNicPath, nic)
        if not os.path.isdir(nicPath):
            continue
        state = ""
        with open(os.path.join(nicPath, "operstate"), "r") as stateFile:
            state = stateFile.read().strip()
        if state != "up":
            continue
        modulePath = os.path.join(nicPath, "device/driver/module")
        module = os.path.basename(os.path.realpath(modulePath))
        nics[nic] = module

    irqs = procfs.interrupts()

    irqStatus = []
    for key, value in irqs.items().items():
        try:
            number = int(key)
        except ValueError:
            continue

        def arr2hex(array: set[int]) -> str:
            resuserst = 0
            for value in array:
                resuserst |= 1 << value
            return f"0x{resuserst:x}"

        affinity = arr2hex(value["affinity"])
        events = sum(value["cpu"])
        userList = value["users"]
        users = []
        for user in userList:
            try:
                modules = nics[user]
                users.append(f"{user}({modules})")
            except KeyError:
                users.append(user)

        pidRegex = re.compile("(irq/%s-.+|IRQ-%s)" % (key, key))
        try:
            pid = pidstats.find_by_regex(pidRegex)[0]
            rtPriority = pidstats[pid]["stat"]["rt_priority"]
            scheduler = schedStr(pid)
            irqStatus.append(
                {
                    "irq": number,
                    "pid": pid,
                    "rtPriority": rtPriority,
                    "scheduler": scheduler,
                    "affinity": affinity,
                    "events": events,
                    "users": users,
                }
            )
        except IndexError:
            irqStatus.append(
                {
                    "irq": number,
                    "affinity": affinity,
                    "events": events,
                    "users": users,
                }
            )

    update["IRQStats"] = irqStatus
    indent = 2 if args.pretty else None
    print(json.dumps(update, indent=indent))


def _handleCPUs(args: argparse.Namespace):
    final = Topology.GetTopology()
    indent = 2 if args.pretty else None
    print(json.dumps(final, indent=indent))


def _getParser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        "json tuna backend",
        exit_on_error=False,
    )
    parser.add_argument(
        "--pretty",
        help="put newlines and indentation in the output",
        action="store_true",
    )
    subparsers = parser.add_subparsers()
    updateCommand = subparsers.add_parser(
        name="update",
        description="update all reportable values",
    )
    updateCommand.set_defaults(func=_handleUpdate)
    cpuCommand = subparsers.add_parser(
        name="topology", description="show the CPU topology"
    )
    cpuCommand.set_defaults(func=_handleCPUs)
    return parser


def backend():
    parser = _getParser()
    try:
        args = parser.parse_args()
    except argparse.ArgumentError as e:
        print(json.dumps(str(e)), file=sys.stderr)
        sys.exit(-1)

    try:
        args.func(args)
    except AttributeError:
        parser.print_help(file=sys.stderr)
        sys.exit(-1)


if __name__ == "__main__":
    backend()
