/*
 *
 * Copyright 2022-2023 Guy Streeter
 *   This copyrighted material is made available to anyone wishing to use,
 *  modify, copy, or redistribute it subject to the terms and conditions of
 *  the GNU General Public License v.3.
 *
 *   This application is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 * Authors:
 *   Guy Streeter <guy.streeter@gmail.com>
 *
 */

import React, { useContext, useEffect, useRef, useState } from 'react';
import {
    Checkbox,
    EmptyState,
    Flex,
    FlexItem,
    Progress,
    ProgressSize,
} from "@patternfly/react-core";
import { TunaContext } from './TunaContext.jsx';
import BitSet from 'bitset';

const ToggleCPUs = ({ cpus, selectedCPUs, setSelectedCPUs }) => {
    const myCPUs = useRef(new BitSet(cpus));
    const [isChecked, setIsChecked] = useState(null);
    useEffect(() => {
        const mask = myCPUs.current.and(selectedCPUs);
        let checked = null;
        if (mask.isEmpty()) {
            checked = false;
        } else if (mask.equals(myCPUs.current)) {
            checked = true;
        }
        setIsChecked(checked);
    }, [selectedCPUs]);
    const handleChange = (checked) => {
        if (checked) {
            setSelectedCPUs(selectedCPUs.or(myCPUs.current));
        } else {
            setSelectedCPUs(selectedCPUs.andNot(myCPUs.current));
        }
        setIsChecked(checked);
    };
    return (
        <Checkbox
            className='cpu-toggle'
            isChecked={isChecked}
            onChange={handleChange}
            id={cpus.toString()}
        />
    );
};

const PUs = ({ structure, stats, ...props }) => {
    const pus = [];
    for (const [number, substruct] of Object.entries(structure)) {
        const usage = number in stats ? stats[number] : 0;
        const title = (
            <>
                <ToggleCPUs cpus={[Number.parseInt(number)]} {...props} />
                {`Processor ${number}`}
            </>
        );
        pus.push(
            <Progress
                key={number}
                size={ProgressSize.sm}
                value={usage}
                title={title}
                measureLocation='inside'
                className={
                    substruct.online
                        ? 'pu-box'
                        : 'pu-box-disabled'
                }
            />
        );
    }
    return pus;
};

const CPUs = ({ structure, ...props }) => {
    if ('CPUs' in structure) {
        return (
            <Flex
                className='cpu-box'
                spacer={{ default: 'spacerXs' }}
                direction={{ default: 'row' }}
            >
                <PUs structure={structure.CPUs} {...props} />
            </Flex>
        );
    }
    return <></>;
};

const Cores = ({ structure, ...props }) => {
    if ('Cores' in structure) {
        const coreArray = Object.entries(structure.Cores);
        const coreClass = coreArray.length > 1 ? 'multi-box' : null;
        const cores = coreArray.map(([number, substruct]) => {
            return (
                <FlexItem className={coreClass} key={number}>
                    <ToggleCPUs cpus={substruct.CPUList} {...props} />
                    {`Core ${number}`}
                    <Caches structure={substruct} {...props} />
                </FlexItem>
            );
        });
        return (
            <Flex className='core-box' direction={{ default: 'row' }}>
                {cores}
            </Flex>
        );
    }
    return <CPUs structure={structure} {...props} />;
};

const Caches = ({ structure, ...props }) => {
    const caches = [];
    if ('Caches' in structure) {
        const cacheArray = Object.entries(structure.Caches);
        for (const [level, typeStruct] of cacheArray) {
            if (level === 'CPUs') {
                continue;
            }
            for (const [type, idStruct] of Object.entries(typeStruct)) {
                for (const [id, substruct] of Object.entries(idStruct)) {
                    const key = `${level}${type}${id}`;
                    caches.unshift(
                        <FlexItem
                            key={key}
                            className='cache-box'
                        >
                            <ToggleCPUs cpus={substruct.CPUList} {...props} />
                            {substruct.Label}
                        </FlexItem>
                    );
                }
            }
        }
        return (
            <>
                {caches}
                <Cores structure={structure} {...props} />
            </>
        );
    }
    return <Cores structure={structure} {...props} />;
};

const Sockets = ({ structure, ...props }) => {
    const subArray = Object.entries(structure.Sockets);
    const sockets = subArray.map(([number, substruct]) => {
        return (
            <FlexItem key={`s${number}`} className='socket-box'>
                <ToggleCPUs cpus={substruct.CPUList} {...props} />
                {`Socket ${number}`}
                <Caches structure={substruct} {...props} />
            </FlexItem>
        );
    });
    return sockets;
};

const Nodes = ({ structure, ...props }) => {
    const nodes = [];
    for (const [number, substruct] of Object.entries(structure.Nodes)) {
        nodes.push(
            <React.Fragment key={number}>
                <FlexItem key={number} spacer={{ default: 'spacerNone' }} className="node-box">
                    <ToggleCPUs cpus={substruct.CPUList} {...props} />
                    {`Node ${number}`}
                </FlexItem>
                <Sockets structure={substruct} {...props} />
            </React.Fragment>
        );
    }
    return <Flex display={{ default: 'inlineFlex' }} direction={{ default: 'column' }}>{nodes}</Flex>;
};

const Topology = ({ cpuTopology, cpuStats }) => {
    const previousStats = useRef({});
    const previousUsage = useRef({});
    const [totalCPU, setTotalCPU] = useState(0);
    const { selectedCPUs, setSelectedCPUs, setTotalUsage } = useContext(TunaContext);

    useEffect(() => {
        if ('CPUList' in cpuTopology) {
            setSelectedCPUs(new BitSet(cpuTopology.CPUList));
        }
    }, [cpuTopology, setSelectedCPUs]);

    useEffect(() => {
        setTotalUsage(totalCPU);
    }, [setTotalUsage, totalCPU]);

    if (Object.keys(cpuTopology).length === 0) {
        return <EmptyState />;
    }

    if ('time' in previousStats.current) {
        const deltaTime = cpuStats.time - previousStats.current.time;
        const cpuUsage = {};
        if (deltaTime !== 0) {
            for (const [cpu, usage] of Object.entries(cpuStats.cpus)) {
                cpuUsage[cpu] = Math.min(100, ((usage - previousStats.current.cpus[cpu]) * 100) / deltaTime);
            }
            previousUsage.current = cpuUsage;
            setTotalCPU(cpuUsage.total);
        }
    }
    previousStats.current = cpuStats;
    return (
        <Nodes
            structure={cpuTopology}
            stats={previousUsage.current}
            selectedCPUs={selectedCPUs}
            setSelectedCPUs={setSelectedCPUs}
        />
    );
};

export default Topology;
