/*
 *
 * Copyright 2022-2023 Guy Streeter
 *   This copyrighted material is made available to anyone wishing to use,
 *  modify, copy, or redistribute it subject to the terms and conditions of
 *  the GNU General Public License v.3.
 *
 *   This application is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 * Authors:
 *   Guy Streeter <guy.streeter@gmail.com>
 *
 */

import React, { useContext, useState } from "react";
import cockpit from "cockpit";
import {
    Button,
    ClipboardCopy,
    Flex,
    FlexItem,
    Form,
    FormGroup,
    Modal,
    Progress,
    ProgressSize,
    Switch,
    TextInput,
    Toolbar,
    ToolbarContent,
    ToolbarItem,
} from "@patternfly/react-core";
import CogIcon from "@patternfly/react-icons/dist/esm/icons/cog-icon";
import { TunaContext } from "./TunaContext.jsx";
import BitSet from "bitset";

const _ = cockpit.gettext;

export const asHexWords = (bitset, wordSize = 32) => {
    if (bitset.isEmpty()) {
        return "0x0";
    }
    const wordCount = Math.ceil((bitset.msb() + 1) / wordSize);
    const array = new Array(wordCount);
    for (let i = 0; i < wordCount; i++) {
        array[i] =
            "0x" +
            bitset
                    .slice(i * wordSize, i * wordSize + wordSize - 1)
                    .toString(16)
                    .toUpperCase();
    }
    return array.reverse().join(",");
};

export const fromHexWords = (hexString, wordSize = 32) => {
    const bitset = new BitSet();
    hexString
            .split(",")
            .reverse()
            .forEach((entry, index) => {
                const slice = BitSet(entry).toArray();
                slice.forEach((value) => {
                    bitset.set(value + index * wordSize);
                });
            });
    return bitset;
};

// https://rosettacode.org/wiki/Range_extraction#Idiomatic
export const arrayToRange = (array) => {
    const ranges = [];
    const sorted = [...array.filter(Number.isInteger)].sort((x, y) =>
        Math.sign(x - y)
    );
    const sequenceBreak = (x, y) => y - x > 1;

    let i = 0;
    while (i < sorted.length) {
        let j = i;
        while (
            j < sorted.length - 1 &&
            !sequenceBreak(sorted[j], sorted[j + 1])
        ) {
            ++j;
        }

        const from = sorted[i];
        const thru = sorted[j];
        const rangeLen = 1 + j - i;

        if (from === thru) {
            ranges.push([from]);
        } else {
            if (rangeLen > 2) {
                ranges.push([from, thru]);
            } else {
                ranges.push([from], [thru]);
            }
        }

        i = j + 1;
    }
    return ranges.map((range) => range.join("-")).join(",");
};

// https://rosettacode.org/wiki/Range_expansion#ES5
export const rangeToArray = (string) => {
    // [m..n]
    const range = (m, n) => {
        return Array.apply(null, Array(n - m + 1)).map(function (x, i) {
            return m + i;
        });
    };

    // concat map yields flattened output list
    return [].concat.apply(
        [],
        string
                .split(",")
                .map(function (x) {
                    return x.split("-").reduce(function (a, s, i, l) {
                        // negative (after item 0) if preceded by an empty string
                        // (i.e. a hyphen-split artefact, otherwise ignored)
                        return s.length
                            ? i
                                ? a.concat(
                                    parseInt(l[i - 1].length ? s : "-" + s, 10)
                                )
                                : [+s]
                            : a;
                    }, []);

                    // two-number lists are interpreted as ranges
                })
                .map(function (r) {
                    return r.length > 1 ? range.apply(null, r) : r;
                })
    );
};

export const shortestBitSpec = (bitset) => {
    if (typeof bitset === "string") {
        bitset = fromHexWords(bitset);
    }
    const range = arrayToRange(bitset.toArray());
    const cardinality = bitset.cardinality();
    if (range.length < cardinality) {
        return range;
    }
    const hex = asHexWords(bitset);
    if (bitset.msb() < cardinality) {
        return hex;
    }
    if (hex.length > range.length) {
        return range;
    }
    return hex;
};

const TunaToolbar = () => {
    const Settings = () => {
        const [isModalOpen, setIsModalOpen] = useState(false);
        const { refreshInterval, setRefreshInterval } = useContext(TunaContext);
        const [refreshInputValue, setRefreshInputValue] =
            useState(refreshInterval);
        const handleConfirm = () => {
            setIsModalOpen(false);
            if (refreshInputValue !== "") {
                setRefreshInterval(refreshInputValue);
            }
        };
        const handleCancel = () => {
            setIsModalOpen(false);
        };
        const handleRefreshChange = (value) => {
            if (value === "") {
                setRefreshInputValue("");
                return;
            }
            value = parseInt(value);
            if (value < 1) {
                value = 1;
            }
            setRefreshInputValue(value);
        };
        const handleSettingsButton = () => {
            setRefreshInputValue(refreshInterval);
            setIsModalOpen(true);
        };
        return (
            <>
                <Button variant="plain" onClick={handleSettingsButton}>
                    <CogIcon /> {_("Settings")}
                </Button>
                <Modal
                    title={_("Settings")}
                    variant="small"
                    position="top"
                    isOpen={isModalOpen}
                    showClose={false}
                    actions={[
                        <Button
                            key="confirm"
                            type="button"
                            variant="primary"
                            onClick={handleConfirm}
                        >
                            {_("Confirm")}
                        </Button>,
                        <Button
                            key="cancel"
                            type="button"
                            variant="secondary"
                            isDanger
                            onClick={handleCancel}
                        >
                            {_("Cancel")}
                        </Button>,
                    ]}
                >
                    <Form onSubmit={(e) => e.preventDefault()}>
                        <FormGroup
                            label={_("Refresh Interval in Seconds")}
                            isRequired
                        >
                            <TextInput
                                className="refresh-input"
                                id="Refresh Interval in Seconds"
                                isRequired
                                type="number"
                                value={refreshInputValue}
                                onChange={handleRefreshChange}
                            />
                        </FormGroup>
                    </Form>
                </Modal>
            </>
        );
    };
    const RefreshSwitch = () => {
        const { isRefreshing, setIsRefreshing } = useContext(TunaContext);
        return (
            <Switch
                label=<b>{_("Refresh on")}</b>
                labelOff={_("Refresh off")}
                isChecked={isRefreshing}
                onChange={setIsRefreshing}
            />
        );
    };
    const SelectedCPUsText = () => {
        const { selectedCPUs } = useContext(TunaContext);
        return (
            <Flex
                spaceItems={{ default: "spaceItemsNone" }}
                display={{ default: "inlineFlex" }}
            >
                <FlexItem className="cpu-mask-text">
                    {_("CPU mask")}
                </FlexItem>
                <FlexItem>
                    <ClipboardCopy>
                        {shortestBitSpec(selectedCPUs)}
                    </ClipboardCopy>
                </FlexItem>
            </Flex>
        );
    };
    const TotalUsage = () => {
        const { totalUsage } = useContext(TunaContext);
        return (
            <Progress
                className='total-cpu'
                size={ProgressSize.md}
                value={totalUsage}
                aria-label={_('Total CPU')}
                measureLocation='inside'
            />
        );
    };
    return (
        <Toolbar isSticky>
            <ToolbarContent>
                <ToolbarItem>
                    <SelectedCPUsText />
                </ToolbarItem>
                <ToolbarItem variant="separator" />
                <ToolbarItem>
                    <TotalUsage />
                </ToolbarItem>
                <ToolbarItem alignment={{ default: "alignRight" }}>
                    <RefreshSwitch />
                </ToolbarItem>
                <ToolbarItem alignment={{ default: "alignRight" }}>
                    <Settings />
                </ToolbarItem>
            </ToolbarContent>
        </Toolbar>
    );
};

export default TunaToolbar;
