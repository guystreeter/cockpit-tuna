/*
 *
 * Copyright 2022-2023 Guy Streeter
 *   This copyrighted material is made available to anyone wishing to use,
 *  modify, copy, or redistribute it subject to the terms and conditions of
 *  the GNU General Public License v.3.
 *
 *   This application is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 * Authors:
 *   Guy Streeter <guy.streeter@gmail.com>
 *
 */

import BitSet from 'bitset';
import React, { useState, createContext } from 'react';

export const TunaContext = createContext();

export const TunaContextProvider = props => {
    const [filterString, setFilterString] = useState('');
    const [refreshInterval, setRefreshInterval] = useState(3);
    const [isRefreshing, setIsRefreshing] = useState(true);
    const [showKernelThreads, setShowKernelThreads] = useState(true);
    const [showUserThreads, setShowUserThreads] = useState(true);
    const [processList, setProcessList] = useState({});
    const [selectedCPUs, setSelectedCPUs] = useState(new BitSet());
    const [totalUsage, setTotalUsage] = useState(0);

    return (
        <TunaContext.Provider
            value={{
                filterString,
                setFilterString,
                refreshInterval,
                setRefreshInterval,
                isRefreshing,
                setIsRefreshing,
                showKernelThreads,
                setShowKernelThreads,
                showUserThreads,
                setShowUserThreads,
                processList,
                setProcessList,
                selectedCPUs,
                setSelectedCPUs,
                totalUsage,
                setTotalUsage
            }}
        >
            {props.children}
        </TunaContext.Provider>
    );
};
