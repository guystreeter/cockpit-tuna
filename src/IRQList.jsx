/*
 *
 * Copyright 2023 Guy Streeter
 *   This copyrighted material is made available to anyone wishing to use,
 *  modify, copy, or redistribute it subject to the terms and conditions of
 *  the GNU General Public License v.3.
 *
 *   This application is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 * Authors:
 *   Guy Streeter <guy.streeter@gmail.com>
 *
 */

import React from "react";
import {
    EmptyState,
    EmptyStateIcon,
    Spinner,
    Title
} from "@patternfly/react-core";
import {
    TableComposable,
    Tbody,
    Td,
    Th,
    Thead,
    Tr,
} from "@patternfly/react-table";
import cockpit from 'cockpit';
import { shortestBitSpec } from "./TunaToolbar.jsx";

const _ = cockpit.gettext;

const tableHeaders = {
    irq: _('IRQ'),
    pid: _('Process ID'),
    scheduler: _('Kernel Scheduler Policy'),
    rtPriority: _('Realtime Priority'),
    affinity: _('CPU Affinity'),
    events: _('Events'),
    users: _('Users'),
};

const tableParsers = {
    irq: value => value,
    pid: value => value,
    scheduler: value => value,
    rtPriority: value => value,
    affinity: shortestBitSpec,
    events: value => value,
    users: value => value,
};

const IrqTableHeader = () => {
    const cells = [];
    for (const [key, value] of Object.entries(tableHeaders)) {
        cells.push(
            <Th
                key={key}
                modifier='wrap'
            >
                {value}
            </Th>
        );
    }
    return (
        <Thead><Tr>{cells}</Tr></Thead>
    );
};

const TunaIrqTable = ({ currentStats, previousStats }) => {
    if (currentStats.length === 0) {
        return (
            <EmptyState>
                <EmptyStateIcon variant="container" component={Spinner} />
                <Title size='lg' headingLevel='h4'>
                    Loading
                </Title>
            </EmptyState>
        );
    }

    const entries = currentStats.map(entry => {
        const row = [];
        for (const [key, parser] of Object.entries(tableParsers)) {
            const previousRow = previousStats.find(p => p.irq === entry.irq);
            const isNewValue = index => {
                if (index === 'users') {
                    if (previousRow === undefined) {
                        return true;
                    }
                    const newArray = entry.users;
                    const oldArray = previousRow.users;
                    const equals = newArray.length === oldArray.length &&
                        newArray.every((v, i) => v === oldArray[i]);
                    return !equals;
                }
                return previousRow?.[index] !== entry[index];
            };
            const id = isNewValue(key) ? 'new-value' : null;
            row.push(
                <Td
                    id={id}
                    key={key}
                    modifier='nowrap'
                >
                    {key in entry ? parser(entry[key]) : null}
                </Td>
            );
        }
        return (
            <Tr key={entry.irq}>{row}</Tr>
        );
    });

    return (
        <TableComposable
            variant="compact"
            borders
            className="tuna--table"
        >
            <IrqTableHeader />
            <Tbody>
                {entries}
            </Tbody>
        </TableComposable>
    );
};

export default TunaIrqTable;
