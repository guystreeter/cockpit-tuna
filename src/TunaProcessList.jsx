/*
 *
 * Copyright 2022-2023 Guy Streeter
 *   This copyrighted material is made available to anyone wishing to use,
 *  modify, copy, or redistribute it subject to the terms and conditions of
 *  the GNU General Public License v.3.
 *
 *   This application is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 * Authors:
 *   Guy Streeter <guy.streeter@gmail.com>
 *
 */

import React, { useState, useEffect, useContext } from 'react';
import {
    TableComposable,
    Thead,
    Tr,
    Th,
    Tbody,
    Td,
    ExpandableRowContent,
} from '@patternfly/react-table';
import { EmptyStatePanel } from "cockpit-components-empty-state.jsx";
import SearchIcon from '@patternfly/react-icons/dist/esm/icons/search-icon';
import { TunaContext } from './TunaContext.jsx';
import cockpit from 'cockpit';
import {
    Button,
    TextInputGroup,
    TextInputGroupMain,
    TextInputGroupUtilities,
    ToggleGroup,
    ToggleGroupItem,
    Toolbar,
    ToolbarContent,
    ToolbarItem,
    Truncate
} from '@patternfly/react-core';
import TimesIcon from '@patternfly/react-icons/dist/esm/icons/times-icon';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon';
import globalDangerColor200 from '@patternfly/react-tokens/dist/esm/global_danger_color_200';
import { shortestBitSpec } from './TunaToolbar.jsx';

const _ = cockpit.gettext;

const ThreadButtonGroup = () => {
    const { showKernelThreads, setShowKernelThreads, showUserThreads, setShowUserThreads } = useContext(TunaContext);
    const handleShowUser = value => {
        if (!value) setShowKernelThreads(true);
        setShowUserThreads(value);
    };
    const handleShowKernel = value => {
        if (!value) setShowUserThreads(true);
        setShowKernelThreads(value);
    };
    return (
        <ToggleGroup>
            <ToggleGroupItem
                text={_('Show kernel threads')}
                isSelected={showKernelThreads}
                onChange={handleShowKernel}
            />
            <ToggleGroupItem
                text={_('Show user threads')}
                isSelected={showUserThreads}
                onChange={handleShowUser}
            />
        </ToggleGroup>
    );
};

const RegexInputGroup = () => {
    const [showError, setShowError] = useState(false);
    const { filterString, setFilterString } = useContext(TunaContext);
    const [showClearButton, setShowClearButton] = useState(false);
    const handleChange = value => {
        try {
            RegExp(value);
            setShowError(false);
        } catch {
            setShowError(true);
        }
        setShowClearButton(value !== '');
        setFilterString(value);
    };
    return (
        <TextInputGroup>
            <TextInputGroupMain
                placeholder={_('Commandline regex')}
                icon={showError
                    ? <ExclamationCircleIcon color={globalDangerColor200.value} />
                    : <SearchIcon />}
                value={filterString}
                onChange={handleChange}
            />
            {showClearButton && (
                <TextInputGroupUtilities>
                    <Button
                        variant='plain'
                        onClick={() => handleChange('')}
                    >
                        <TimesIcon />
                    </Button>
                </TextInputGroupUtilities>
            )}
        </TextInputGroup>
    );
};

const ProcessToolbar = () => {
    return (
        <Toolbar isSticky>
            <ToolbarContent>
                <ToolbarItem>
                    <RegexInputGroup />
                </ToolbarItem>
                <ToolbarItem>
                    <ThreadButtonGroup />
                </ToolbarItem>
            </ToolbarContent>
        </Toolbar>
    );
};

const tableHeaders = {
    pid: _('Process ID'),
    policy: _('Kernel Scheduler Policy'),
    rtPriority: _('Realtime Priority'),
    affinity: _('CPU Affinity'),
    lastCPU: _('Last CPU'),
    volCtxSwitch: _('Volunrary Context Switches'),
    nonVolCtxSwitch: _('Non-voluntary Switches'),
    commandLine: _('Process Commandline'),
};

const tableParsers = {
    pid: value => value,
    policy: value => value,
    rtPriority: value => value,
    affinity: shortestBitSpec,
    lastCPU: value => value,
    volCtxSwitch: value => value,
    nonVolCtxSwitch: value => value,
    commandLine: value => <Truncate content={value} />,
};

function ProcTableHeader() {
    const ExpCol = [<Th className='pf-m-fit-content' key='blank' />];
    const cells = [];
    for (const [key, value] of Object.entries(tableHeaders)) {
        cells.push(
            <Th
                key={key}
                modifier='wrap'
            >
                {value}
            </Th>
        );
    }
    return (
        <Thead><Tr>{ExpCol}{cells}</Tr></Thead>
    );
}

function ProcTableRow({ row, previousRow, isExpanded, onExpandToggle }) {
    const cols = [];
    const isNewValue = index => previousRow?.[index] !== row[index];
    const isNewThreadValue = (row, tid, index) => {
        if (!previousRow?.threads) return true;
        const prevThread = previousRow.threads.find(thread => thread.pid === tid);
        if (!prevThread) return true;
        const newThread = row.threads.find(thread => thread.pid === tid);
        return prevThread[index] !== newThread[index];
    };

    for (const [key, parser] of Object.entries(tableParsers)) {
        const id = isNewValue(key) ? 'new-value' : null;
        cols.push(
            <Td
                key={key}
                id={id}
                modifier='nowrap'
            >
                {parser(row[key])}
            </Td>
        );
    }
    let threadRows;
    if ("threads" in row) {
        threadRows = row.threads.map(trow => {
            const tcols = [<Td key='blank' />];
            for (const key of Object.keys(tableHeaders)) {
                const id = isNewThreadValue(row, trow.pid, key) ? 'new-value' : null;
                tcols.push(
                    <Td
                        key={key}
                        id={id}
                        modifier='nowrap'
                    >
                        <ExpandableRowContent>
                            {trow[key]}
                        </ExpandableRowContent>
                    </Td>
                );
            }
            return (
                <Tr key={trow.pid} isExpanded={isExpanded}>
                    {tcols}
                </Tr>
            );
        });
    }
    return (
        <>
            <Tr>
                <Td
                    expand={"threads" in row
                        ? {
                            isExpanded,
                            rowIndex: row.pid,
                            onToggle: () => onExpandToggle(row.pid),
                            expandId: 'threads',
                        }
                        : undefined}
                />
                {cols}
            </Tr>
            {threadRows}
        </>
    );
}

function ProcTableBodies({ filteredcurrentList, previousList, isRowExpanded, onExpandToggle }) {
    const bodies = filteredcurrentList.map((row) => {
        const previousRow = previousList.find(p => p.pid === row.pid);
        const isExpanded = isRowExpanded(row.pid);
        return (
            <Tbody key={row.pid} isExpanded={isExpanded}>
                <ProcTableRow
                    key={row.pid}
                    row={row}
                    previousRow={previousRow}
                    isExpanded={isExpanded}
                    onExpandToggle={onExpandToggle}
                />
            </Tbody>
        );
    });
    return (<>{bodies}</>);
}

export default function TunaProcTable({ currentList, previousList }) {
    const {
        filterString,
        showKernelThreads,
        showUserThreads
    } = useContext(TunaContext);
    // we want to re-render if these change
    const [expandedPIDs, setExpandedPIDs] = useState([]);
    const [filteredList, setFilteredList] = useState([]);

    useEffect(() => {
        try {
            const re = RegExp(filterString);
            setFilteredList(currentList.filter(row => {
                if (row.isKthread && (!showKernelThreads)) return false;
                if ((!row.isKthread) && (!showUserThreads)) return false;
                return re.test(row.commandLine);
            }));
        } catch { }
    }, [filterString, showKernelThreads, showUserThreads, currentList]);

    // add or remove the PID from the list of expanded rows.
    function handleExpandToggle(pid) {
        let newExpandedPIDs = [];
        if (expandedPIDs.includes(pid)) {
            newExpandedPIDs = expandedPIDs.filter(p => p !== pid);
        } else {
            newExpandedPIDs = [...expandedPIDs, pid];
        }
        // cause a re-render
        setExpandedPIDs(newExpandedPIDs);
    }

    function isRowExpanded(pid) {
        return expandedPIDs.includes(pid);
    }

    let procTable;
    // show a loading icon before the first backend result arrives.
    if (currentList.length === 0) {
        procTable = <EmptyStatePanel loading />;
    } else if (filteredList.length === 0) {
        procTable = <EmptyStatePanel icon={SearchIcon} title={_("No match for filters")} />;
    } else {
        procTable = (
            <TableComposable
                isExpandable
                variant='compact'
                borders
                className='tuna--table'
            >
                <ProcTableHeader nowrap tableHeaders={tableHeaders} />
                <ProcTableBodies
                    filteredcurrentList={filteredList}
                    previousList={previousList}
                    onExpandToggle={handleExpandToggle}
                    isRowExpanded={isRowExpanded}
                />
            </TableComposable>
        );
    }
    return (
        <>
            <ProcessToolbar />
            {procTable}
        </>
    );
}
