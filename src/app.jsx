/*
 *
 * Copyright 2022-2023 Guy Streeter
 *   This copyrighted material is made available to anyone wishing to use,
 *  modify, copy, or redistribute it subject to the terms and conditions of
 *  the GNU General Public License v.3.
 *
 *   This application is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 * Authors:
 *   Guy Streeter <guy.streeter@gmail.com>
 *
 */

import React, { useContext, useEffect, useRef, useState } from 'react';
import {
    Alert,
    AlertGroup,
    BackToTop,
    Stack,
    StackItem,
    Tab,
    TabContent,
    TabContentBody,
    Tabs,
    TabTitleText
} from '@patternfly/react-core';
import TunaToolbar from './TunaToolbar.jsx';
import TunaProcTable from './TunaProcessList.jsx';
import { TunaContext, TunaContextProvider } from './TunaContext.jsx';
import cockpit from 'cockpit';
import Topology from './CPUTopology.jsx';
import TunaIrqTable from './IRQList.jsx';

const _ = cockpit.gettext;

const MainStackItem = () => {
    const [processList, setProcessList] = useState([]);
    const [cpuStats, setCpuStats] = useState({});
    const [cpuTopology, setCpuTopology] = useState({});
    const [irqStats, setIrgStats] = useState([]);
    const [alerts, setAlerts] = useState([]);
    const {
        isRefreshing,
        setIsRefreshing,
        refreshInterval,
    } = useContext(TunaContext);

    // this needs to stick around for subsequent renders
    const timeoutID = useRef(null);
    const previousList = useRef([]);
    const previousIRQs = useRef([]);

    cockpit.onvisibilitychange = () => {
        if (cockpit.hidden) {
            setIsRefreshing(false);
        }
    };
    const addAlert = alert => setAlerts(prevAlerts => [...prevAlerts, alert]);

    useEffect(() => {
        const command = [
            'python3',
            '-m',
            'cockpit_tuna',
            'topology'
        ];
        cockpit.spawn(
            command,
            { err: 'message' }
        )
                .then(data => {
                    setCpuTopology(JSON.parse(data));
                })
                .catch(exception => {
                    addAlert({ message: exception.message, key: 'topology' });
                    throw new Error('backend topology command failed');
                });
    }, []);

    useEffect(() => {
        const updateStats = () => {
            cockpit.spawn(
                [
                    'python3',
                    '-m',
                    'cockpit_tuna',
                    'update',
                ],
                { err: "message" }
            )
                    .then(data => {
                        previousList.current = processList;
                        previousIRQs.current = irqStats;
                        const parsed = JSON.parse(data);
                        setProcessList(parsed.Processes);
                        setCpuStats(parsed.CPUstats);
                        setIrgStats(parsed.IRQStats);
                    })
                    .catch((exception) => {
                        addAlert({ message: exception.message, key: 'update' });
                        throw new Error('backend update command failed');
                    });
        };

        clearTimeout(timeoutID.current);
        if (isRefreshing) {
            if (timeoutID.current === null) {
                timeoutID.current = -1;
                updateStats();
            } else {
                timeoutID.current = setTimeout(
                    updateStats,
                    refreshInterval * 1000
                );
            }
        } else {
            timeoutID.current = null;
        }
    }, [isRefreshing, refreshInterval, processList, irqStats]);

    // This makes sure we don't leave a timer hanging when we exit
    useEffect(() => () => {
        return () => clearTimeout(timeoutID.current);
    }, []);

    return (
        <StackItem isFilled>
            <AlertGroup isToast>
                {alerts.map(entry => {
                    return (
                        <Alert
                            isExpandable
                            isLiveRegion
                            variant='danger'
                            title={_('Interaction with backend server failed')}
                            key={entry.key}
                        >
                            <p>{entry.message}</p>
                        </Alert>
                    );
                })}
            </AlertGroup>
            <Tabs defaultActiveKey='processes'>
                <Tab
                    eventKey='processes'
                    title={<TabTitleText>{_('Processes')}</TabTitleText>}
                >
                    <TabContent>
                        <TabContentBody>
                            <TunaProcTable
                                currentList={processList}
                                previousList={previousList.current}
                            />
                        </TabContentBody>
                    </TabContent>
                </Tab>
                <Tab
                    eventKey='irqs'
                    title={<TabTitleText>{_('IRQs')}</TabTitleText>}
                >
                    <TunaIrqTable
                        currentStats={irqStats}
                        previousStats={previousIRQs.current}
                    />
                </Tab>
                <Tab
                    eventKey='cpus'
                    title={<TabTitleText>{_('CPUs')}</TabTitleText>}
                >
                    <Topology cpuTopology={cpuTopology} cpuStats={cpuStats} />
                </Tab>
            </Tabs>
        </StackItem>
    );
};

export function Application() {
    return (
        <div id='main-scroll-div'>
            <Stack hasGutter>
                <TunaContextProvider>
                    <StackItem>
                        <TunaToolbar />
                    </StackItem>
                    <MainStackItem />
                </TunaContextProvider>
                <BackToTop scrollableSelector='[id="main-scroll-div"]' />
            </Stack>
        </div>
    );
}
