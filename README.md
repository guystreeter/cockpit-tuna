<!---
// Copyright 2023 Guy Streeter
//   This copyrighted material is made available to anyone wishing to use,
//  modify, copy, or redistribute it subject to the terms and conditions of
//  the GNU General Public License v.3.
//
//   This application is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   General Public License for more details.
//
// Authors:
//   Guy Streeter <guy.streeter@gmail.com>
--->

# Cockpit Tuna Plugin

A [Cockpit](https://cockpit-project.org/) module like the Tuna GUI

# Packages

RPM packages for Fedora and Centos-stream are available in
[copr](https://copr.fedorainfracloud.org/coprs/streeter/cockpit-tuna/)

# Getting and building the source

Make sure you have `npm` available (usually from your distribution package).
These commands check out the source and build it into the `dist/` directory:

```
git clone https://gitlab.com/guystreeter/cockpit-tuna.git
cd cockpit-tuna
make
```

# Installing

`make install` compiles and installs the package in `/usr/local/share/cockpit/`. The
convenience targets `srpm` and `rpm` build the source and binary rpms,
respectively. Both of these make use of the `dist` target, which is used
to generate the distribution tarball. In `production` mode, source files are
automatically minified and compressed. Set `NODE_ENV=production` if you want to
duplicate this behavior.

For development, you usually want to run your module straight out of the git
tree. To do that, run `make devel-install`, which bind-mounts your dist directory to the
location were cockpit-bridge looks for packages. `make devel-uninstall` will undo this.

After changing the code and running `make` again, reload the Cockpit page in
your browser.

You can also use
[watch mode](https://webpack.js.org/guides/development/#using-watch-mode) to
automatically update the webpack on every code change with

    $ npm run watch

or

    $ make watch

# Running eslint

This uses [ESLint](https://eslint.org/) to automatically check
JavaScript code style in `.js` and `.jsx` files.

The linter is executed within every build as a webpack preloader.

For developer convenience, the ESLint can be started explicitly by:

    $ npm run eslint

Violations of some rules can be fixed automatically by:

    $ npm run eslint:fix

Rules configuration can be found in the `.eslintrc.json` file.

## Running stylelint

Cockpit uses [Stylelint](https://stylelint.io/) to automatically check CSS code
style in `.css` and `scss` files.

The linter is executed within every build as a webpack preloader.

For developer convenience, the Stylelint can be started explicitly by:

    $ npm run stylelint

Violations of some rules can be fixed automatically by:

    $ npm run stylelint:fix

Rules configuration can be found in the `.stylelintrc.json` file.

During fast iterative development, you can also choose to not run stylelint.
This speeds up the build and avoids build failures due to e. g. ill-formatted
css or other issues:

    $ make STYLELINT=0
